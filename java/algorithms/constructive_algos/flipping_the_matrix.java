/*
Sean invented a game involving a  matrix where each cell of the matrix contains an integer. He can reverse any of its rows or columns any number of times, and the goal of the game is to maximize the sum of the elements in the  submatrix located in the upper-left corner of the  matrix (i.e., its upper-left quadrant).

Given the initial configurations for  matrices, help Sean reverse the rows and columns of each matrix in the best possible way so that the sum of the elements in the matrix's upper-left quadrant is maximal. For each matrix, print the maximized sum on a new line.
*/

import java.io.*;
import java.util.*;

public class Solution {
  
    public static int[][] createMatrix(int row, int column, Scanner input){
      int[][] matrix = new int[row][column];
      for(int i = 0; i < row; i++){
        for(int j = 0; j < column; j++){
          matrix[i][j] = input.nextInt();
        }
      }
      return matrix;
    }
   
    public static int sumUpQuadrant(int[][]matrix, int n){
      int m = 2*n;
      int sum = 0;
      for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
          sum +=max(matrix[i][j],matrix[i][m-1-j],matrix[m-1-i][j],matrix[m-1-i][m-1-j]);
        }
      }
      return sum;
    }
  
    public static int max(int a, int b, int c, int d){
      return  Math.max(Math.max(Math.max(a,b),c),d);
    }
  
  
  
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
     
      Scanner input = new Scanner(System.in);
      int numberOfQueries = input.nextInt();
      for(int x = 0; x < numberOfQueries; x++){
        int n = input.nextInt();
        int row = 2*n;
        int column = 2*n;

        //create matrix
        int[][] matrix = createMatrix(row, column, input);
        int sum = sumUpQuadrant(matrix, n);
        System.out.println(sum);
      }
    }
}
