#!/bin/python3

import sys

def swap(first_val, second_val):
  temp = first_val
  first_val = second_val
  second_val = temp
  
  return(first_val, second_val)

n = int(input().strip())
a = [int(a_temp) for a_temp in input().strip().split(' ')]

num_of_swaps = 0
iterations = 1

while iterations < n:
  for i in range(n-1):
    if a[i] > a[i+1]:
      num_of_swaps += 1
      a[i], a[i+1] = swap(a[i], a[i+1])
  iterations +=1
    
print('Array is sorted in {} swaps.'.format(num_of_swaps))
print('First Element: {}'.format(int(a[0])))
print('Last Element: {}'.format(int(a[n-1])))
